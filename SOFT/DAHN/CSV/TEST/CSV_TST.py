 # -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

import sys
sys.path.append('..')
import CSV

def TEST():
	#Functional test
	CSVSTRING=CSV.LF("TEST.csv")
	print("CSV::STRING::")
	print(CSVSTRING)

	CSVSTRDATA=CSV.STTD(CSVSTRING, ";")
	print("CSV::STRING::VECTOR::")
	print(CSVSTRDATA)

	CSVFLDATA=CSV.SVFV(CSVSTRDATA)
	print("CSV::FLOAT::VECTOR::")
	print(CSVFLDATA)


if __name__ == '__main__':
	TEST()
